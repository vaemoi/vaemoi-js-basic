# Vaemoi JS Basic
A simple generator for starting an es2015+ project bundled with [rollup](https://github.com/rollup/rollup) + [babel](https://github.com/babel/babel) and testing with [tape](https://github.com/substack/tape) + [testdouble](https://github.com/testdouble/testdouble.js).
