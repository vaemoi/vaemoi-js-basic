/* Silly utilities */


/**
 * Checks if a thing is a number
 *
 * @param      {?}   x    That unknown thing
 *
 * @return     {boolean}  True if number, False otherwise
 */
const isNum = (x) => {
  return Number.isInteger(x) && !isNaN(x);
};

/**
 * Checks a group of possibly number like things with isNum
 *
 * @param      {Array}    theArgs  The unknown things
 *
 * @return     {boolean}  True is all values are numbers False, otherwise
 */
const areNums = (...theArgs) => {
  return theArgs.reduce((reduction, current) => {
    return reduction && isNum(current)
  }, true);
};

export default {
  isNum,
  areNums
};
