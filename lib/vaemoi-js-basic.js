import Utils from './utils/utils.js';

const {areNums, isNum} = Utils;

const Mathey = (init_time) => {
  const
    add = (x, y) => {
      if (Utils.areNums(x,y)) {
        return (x + y);
      }
      return NaN;
    },

    subtract = (x, y) => {
      if (areNums(x,y)) {
        return (x - y);
      }
      return NaN;
    },

    multiply = (x, y) => {
      if (areNums(x,y)) {
        return (x * y);
      }
      return NaN;
    },

    divide = (x, y) => {
      if (areNums(x,y)) {
        return (x / y);
      }
      return NaN;
    },

    square = (x) => {
      if (isNum(x)) {
        return Math.pow(x, 2);
      }
      return NaN;
    },

    square_root = (x) => {
      if (Utils.isNum(x)) {
        return Math.sqrt(x);
      }
      return NaN;
    },

    power = (x, y) => {
      if (areNums(x,y)) {
        return Math.pow(x,y);
      }
      return NaN;
    },

    power_root = (x, y) => {
      if (areNums(x,y)) {
        return Math.pow(x,(1/y));
      }
      return NaN;
    };

  return {
    add,
    subtract,
    multiply,
    divide,
    square,
    square_root,
    power,
    power_root,
    startup: init_time
  };
};

export default Mathey(Date.now());
