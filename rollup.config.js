import resolve from 'rollup-plugin-node-resolve'; // Use node packages
import babel from 'rollup-plugin-babel'; // Use es6, react, other goodies (common config in .babelrc)

export default {
  input: `lib/vaemoi-js-basic.js`,
  output: {
    file: `dist/vaemoi-js-basic.js`,
    format: `cjs`
  },
  // Put external dependencies here
  etxernal: [],
  plugins: [
    resolve(),
    babel({
      exclude: `node_modules/**`,
      plugins: [`external-helpers`]
    })
  ]
};
