import test from 'tape';

import VaemoiJSBasic from '../lib/vaemoi-js-basic.js';


test(`----- Should get sum of two numbers -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(VaemoiJSBasic.add(1, 1), 2);
  swear.ok(isNaN(VaemoiJSBasic.add(1, {})));
});

test(`----- Should difference of two numbers -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(VaemoiJSBasic.subtract(1, 1), 0);
  swear.ok(isNaN(VaemoiJSBasic.subtract(1, {})));
});

test(`----- Should get product of numbers -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(2, VaemoiJSBasic.multiply(2, 1));
  swear.ok(isNaN(VaemoiJSBasic.multiply(1, {})));
});

test(`----- Should get the quotient of two numbers -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(2, VaemoiJSBasic.divide(6, 3));
  swear.ok(isNaN(VaemoiJSBasic.divide(1, {})));
});

test(`----- Should the square of a number -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(VaemoiJSBasic.square(2), 4);
  swear.ok(isNaN(VaemoiJSBasic.square({})))
});

test(`----- Should get the square root of a number -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(2, VaemoiJSBasic.square_root(4));
  swear.ok(isNaN(VaemoiJSBasic.square_root({})))
});

test(`----- Should get the exponential of two numbers -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(8, VaemoiJSBasic.power(2, 3));
  swear.ok(isNaN(VaemoiJSBasic.power(2, {})));
});

test(`----- Should get the nth root, given two numbers  -----`, (swear) => {
  const swearJar = 2;

  swear.plan(swearJar);
  swear.equals(2, VaemoiJSBasic.power_root(8, 3));
  swear.ok(isNaN(VaemoiJSBasic.power_root(8, {})));
});

